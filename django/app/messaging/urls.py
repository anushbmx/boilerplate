from django.urls import path, include
from messaging import views

urlpatterns = [
    # path('login/', views.LoginView.as_view(), name='accounts.login'),
    path('<template_key>/', views.RenderMessageTemplate.as_view(), name='message.template'),
    path('<template_key>/raw', views.RenderMessageTemplate.as_view(), name='message.template.raw'),
    # path('reset/password/', views.PasswordResetView.as_view(), name='accounts.reset_password'),
]
