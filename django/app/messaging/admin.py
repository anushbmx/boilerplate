from django.utils.safestring import mark_safe
from django.contrib import admin
from messaging.models import MessageTemplate
from django.urls import reverse


class MessageTemplateAdmin(admin.ModelAdmin):
    ordering = ('template_key',)
    list_display = ['template_key', 'subject', 'from_email', 'get_type_display']
    search_fields = ('subject', 'from_email', 'template_key')
    list_filter = ('type',)
    save_as = True

    readonly_fields = ('rendered_template', 'rendered_template_raw')

    def rendered_template(self, instance):
        if instance.template_key:
            link = "<a href='%s' target='_blank'>%s</a>" % (
                reverse('message.template', args=[instance.template_key]), 'Render Template')
            return mark_safe(link)
        return ''

    # short_description functions like a model field's verbose_name
    rendered_template.short_description = 'Render Template'

    def rendered_template_raw(self, instance):
        if instance.template_key:
            link = "<a href='%s' target='_blank'>%s</a>" % (
                reverse('message.template.raw', args=[instance.template_key]), 'Render Template (RAW)')
            return mark_safe(link)
        return ''

    # short_description functions like a model field's verbose_name
    rendered_template_raw.short_description = 'Render Template (RAW)'


admin.site.register(MessageTemplate, MessageTemplateAdmin)
