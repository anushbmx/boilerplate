# messaging App

Accounts app deals with messaging tasks.

## Tasks

* Email

### How to use Email

* Add Email template key and Template contents from Admin>Messaging>Add Template.
* use Celery task to call the email function, see example.

```
from messaging.tasks import send_email


send_email('email_template',
               subject=(your subject or subject defined in template will be used.),
               context={
                   Context
               },
               emails=[to email or email defined in template will be used.])

```

***Note*** :In case template does not exist, an error log will be raised.
