from celery import task
from messaging.models import MessageTemplate


@task
def send_email(*args, **kwargs):
    return MessageTemplate.send(*args, **kwargs)
