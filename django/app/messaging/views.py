from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from messaging.models import MessageTemplate
from django.template import Context
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import View

class RenderMessageTemplate(LoginRequiredMixin, View):
    def get(self, request, template_key, raw=False, *args, **kwargs):
        message = get_object_or_404(MessageTemplate, template_key=template_key)
        context = Context(message.context)
        if raw:
            return HttpResponse(message.get_body(message.html_template,context))
        return HttpResponse(message.get_body(None, context))
