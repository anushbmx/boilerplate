# Django Application

This is the django application powering packman project.

## Apps

Packman uses a set of pip packages and custom apps to help people like you and me to make the world a better place :D :) .

### pip packages

* **Crispy Forms** : Delivers magic to Django forms with bootstrap.
* **Django Celery Email**:  Email back-end for that uses a Celery queue for out-of-band sending of the messages.
* **Django Celery Beat**: A one of a kind task scheduler.

### Django apps

* **accounts** :  Authentication and Pre Authentication tasks like Registration, Login, Password reset, Password Change, Profile edit.
* **app** :  Django root, houses all django settings and configs.
* **public**   :  Public app used to serve public pages home page etc.
* **messaging** : Deals with emails and other messaging items like templates etc.
