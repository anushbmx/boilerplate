from django.shortcuts import render
from django.views.generic.base import TemplateView


class HomePage(TemplateView):

    def get(self, request, *args, **kwargs):
        context = {'title': 'Django Packman'}
        return render(request, 'index.html', context=context)
