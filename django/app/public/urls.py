from django.conf.urls import url

from public import views

urlpatterns = [
    url(r'^$', views.HomePage.as_view(), name='public.home'),
]
