from django.contrib.auth import login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect
from django.views.generic.edit import FormView, View
from django.views.generic import TemplateView
from django.shortcuts import render
from accounts.forms import UserForm, ProfileForm


class RegisterView(FormView):
    form_class = UserCreationForm
    template_name = 'register.html'

    def form_valid(self, form):
        user = form.save()
        # If submitted information is correct the registration is complete.

        # To login in new registration and redirect to dashboard use this.
        # login(self.request, user)
        return render(self.request, 'registration_complete.html')


class ProfileView(LoginRequiredMixin, TemplateView):
    def get(self, request, *args, **kwargs):
        context = {'title': 'Profile', 'profile': request.user}
        return render(request, 'profile.html', context=context)


class ProfileEditView(LoginRequiredMixin, View):
    form_class = UserCreationForm
    template_name = 'register.html'

    def get(self, request, *args, **kwargs):
        user_form = UserForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)
        context = {'user_form': user_form, 'profile_form': profile_form}
        return render(request, 'profile_edit.html', context=context)

    def post(self, request, *args, **kwargs):
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, instance=request.user.profile)

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            return redirect('accounts.profile')
        else:
            context = {'user_form': user_form, 'profile_form': profile_form}
            return render(request, 'profile_edit.html', context=context)
