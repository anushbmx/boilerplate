from django.urls import path, include
from accounts import views

urlpatterns = [
    # path('login/', views.LoginView.as_view(), name='accounts.login'),
    path('', include('django.contrib.auth.urls')),
    path('register/', views.RegisterView.as_view(), name='accounts.register'),
    path('profile/', views.ProfileView.as_view(), name='accounts.profile'),
    path('profile/edit', views.ProfileEditView.as_view(), name='accounts.profile.edit'),
    # path('reset/password/', views.PasswordResetView.as_view(), name='accounts.reset_password'),
]
