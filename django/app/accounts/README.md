# accounts App

Accounts app performs Authentication and Pre Authentication tasks

## Tasks 

Tasks in accounts app uses in the box functions from ```django.contrib.auth``` and also custom functions.

These functions uses Django's in build functions.

* Login
* Logout
* Reset Password

These functions are defined in ```views.py```

* Profile View
* Profile Edit
* Registration
