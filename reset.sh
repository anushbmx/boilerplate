#!/usr/bin/env bash

if [ ! -z "$(docker ps -q)" ]; then
	$(docker kill $(docker ps -q))
fi

if [ ! -z "$(docker container ls -qa)" ]; then
	docker rm $(docker container ls -qa)
fi

if [ ! -z "$(docker images -qa)" ]; then
	docker rmi $(docker images -qa)
fi

docker network prune -f
docker volume prune -f

if [ -f .env ]; then
	rm -rf .env
fi
